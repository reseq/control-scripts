##Author: Darren Korbie

## script to summarize and merge trim_galore output from quantitative mapping

printf "Barcode \t TrimGaloreReads \t ReadPairsRemoved \t %% Removed \n" > summary.trim_galore_QC.tab.txt

awk ' 
{ 

OFS = "\t";
if (/^Total number of sequences analysed/) {split($0,a,": "); totalreads = a[2]}
else if (/^Number of sequence pairs removed/) {split($0,b,"\\)|\\(| "); pairs =  b[6]; percentage = b[8]; }
else if (/^Deleting both intermediate output files/) {split($0,c," |_"); print c[6], totalreads, pairs, percentage }

}' trim_galore.out2 >> summary.trim_galore_QC.tab.txt 


## script to summarize and merge bowtie2 output from quantitative mapping

printf "Barcode \t Well \t Letter \t Number \t Total Reads \t Map %% \n" > summary.bowtie2_quant_mapping.tab.txt
awk ' 
{ 

OFS = "\t";
if (/^\*/) {split($0,array,"/|_"); printf array[14] "\t" array[15] "\t"; split(array[15],array2,""); printf array2[1] "\t" array2[2] array2[3] "\t"}
else if (/reads/) {split($0,array," "); printf array[1] "\t"}
else if (/overall/) {split($0,array," "); printf array[1] "\n"}

}' bowtie2.out2 >> summary.bowtie2_quant_mapping.tab.txt

## script to summarize contamination data

printf "\n" > summary.contamination.tab.txt
contaminationdata=($( ls results.contamination/ ))

for arrayposition in ${!contaminationdata[*]}
	
do
	awkline=$(grep "${contaminationdata[$arrayposition]%%_*}" well.info)
	exonType=$(echo "$awkline" | awk '{print $3}' )
	
	
	echo ${contaminationdata[$arrayposition]} >> summary.contamination.tab.txt
	printf "expected	$exonType\n" >> summary.contamination.tab.txt
	cat results.contamination/${contaminationdata[$arrayposition]} >> summary.contamination.tab.txt
done
