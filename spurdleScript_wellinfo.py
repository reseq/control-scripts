#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 29 20:32:12 2014

@author: Darren Korbie
"""

######################################
######################################
######################################

def barcodeFill(number):
    id=str(number)    
    if (len(id) == 1):
        barcode='FLD000'+ id
    elif (len(id) == 2):
        barcode='FLD00' + id
    elif (len(id) == 3):
        barcode='FLD0' + id
    return(barcode)

######################################
######################################
######################################

## script to automatically create well.info file

# read Sequences file provided by Spurdle lab and parse into well.info file
'''
Is expected that all sequenceFiles will have this structure:
0	Transfection Dates
1	Position
2	Gene
3	Exon
4	Variant
5	Prediction
6	Barcode
7	Vector Exon 1
8	BRCA Intron 1
9	BRCA Exon
10	BRCA Intron 2
11	Vector Exon 2



'''

import glob
for file in glob.glob('./*transfections.txt'):
    sequencesFile=file[2:]
    outputFileName='well.info'
   
import csv   
with open(sequencesFile, 'r') as theFile:
        reader = csv.reader(theFile, delimiter='\t')
        SequencesFileList = list(reader)

#read info from sequences file into a list

'''

fastqfile (fluidigm barcode ID FLDXXXX)
well	
Exon	# (e.g., B1ex5)
Unspliced	
cDNA	
ExonicSequence	
intron1_included	
intron2_included

'''

import re
wellinfoList=[]
entriesInSequencesFileList=len(SequencesFileList)
SequencesFileListPosition=1
wellinfoListPosition=0

vectorExon1='TCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCA'

vectorExon2='ACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTCGACCAAT'



while (SequencesFileListPosition < entriesInSequencesFileList ):
    wellinfoList.append([])
    # extract barcode, generate proper FLDXXXX

    wellinfoList[wellinfoListPosition].append(barcodeFill(SequencesFileList[SequencesFileListPosition][6]))    
    # extract well
    wellinfoList[wellinfoListPosition].append(SequencesFileList[SequencesFileListPosition][1])  
    '''need to differentiate between brca exons and everything else'''
    #if entry is a BRCA exon
    if re.search('^B', SequencesFileList[SequencesFileListPosition][2]):
        #extract exon number
        wellinfoList[wellinfoListPosition].append(
            SequencesFileList[SequencesFileListPosition][2] + 
            SequencesFileList[SequencesFileListPosition][3]
            )  
        #extract unspliced sequence
        wellinfoList[wellinfoListPosition].append(
        SequencesFileList[SequencesFileListPosition][7] +
        SequencesFileList[SequencesFileListPosition][8] +
        SequencesFileList[SequencesFileListPosition][9] +
        SequencesFileList[SequencesFileListPosition][10] +
        SequencesFileList[SequencesFileListPosition][11] 
        )  
        #extract cDNA
        wellinfoList[wellinfoListPosition].append(
        SequencesFileList[SequencesFileListPosition][7] +
        SequencesFileList[SequencesFileListPosition][9] +
        SequencesFileList[SequencesFileListPosition][11] 
        ) 
        #extract Exonic Sequence
        wellinfoList[wellinfoListPosition].append(
        SequencesFileList[SequencesFileListPosition][9] 
        ) 
        #extract intron1_included sequence
        wellinfoList[wellinfoListPosition].append(
        SequencesFileList[SequencesFileListPosition][7] +
        SequencesFileList[SequencesFileListPosition][8] +
        SequencesFileList[SequencesFileListPosition][9] +
        SequencesFileList[SequencesFileListPosition][11] 
        ) 
        #extract intron2_included sequence
        wellinfoList[wellinfoListPosition].append(
        SequencesFileList[SequencesFileListPosition][7] +
        SequencesFileList[SequencesFileListPosition][9] +
        SequencesFileList[SequencesFileListPosition][10] +
        SequencesFileList[SequencesFileListPosition][11] 
        ) 

    #if entry is anything OTHER than a BRCA exon
    else:
        #extract tempate type, e.g., GFP, cells, cells+lipo
        wellinfoList[wellinfoListPosition].append(
            SequencesFileList[SequencesFileListPosition][2] 
            )  
        #extract unspliced sequence
        wellinfoList[wellinfoListPosition].append(vectorExon1 + vectorExon2)  
        #extract cDNA
        wellinfoList[wellinfoListPosition].append(vectorExon1 + vectorExon2) 
        #extract Exonic Sequence
        wellinfoList[wellinfoListPosition].append('') 
        #extract intron1_included sequence
        wellinfoList[wellinfoListPosition].append(vectorExon1 + vectorExon2) 
        #extract intron2_included sequence
        wellinfoList[wellinfoListPosition].append(vectorExon1 + vectorExon2) 

    SequencesFileListPosition += 1
    wellinfoListPosition += 1

#write output to tab delimited file
header='fastqfile\twell\tExon\tUnspliced\tcDNA\tExonicSequence\tintron1_included\tintron2_included\n'

outputfile  = open(outputFileName, "w")
outputfile.write(header)
writerobject = csv.writer(outputfile, delimiter='\t', lineterminator='\n')
wellinfoListPosition=0


for a, b in enumerate(wellinfoList):
    writerobject.writerow(b)


outputfile.close()    
