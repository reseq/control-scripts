##Author: Darren Korbie

while getopts ":l:t:h" opt; do
  case $opt in
	l)
		re='^[0-9]+$'
		if ! [[ $OPTARG =~ $re ]] ; then
			echo "error: Not a number, option -l requires a trim length" >&2; exit 1
		fi
		echo "-l was triggered for trim_galore, Parameter: $OPTARG" >&2
		trimGaloreLength=$OPTARG
		;;
	t)
		re='^[0-9]+$'
		if ! [[ $OPTARG =~ $re ]] ; then
			echo "error: Not a number, option -t requires a number to specify number of threads to use" >&2; exit 1
		fi
		echo "-t was triggered for the nubmer of cores/threads to utilize" >&2
		numbOfCores=$OPTARG
		;;

   \?)
		echo "Invalid option: -$OPTARG" >&2
		exit 1
		;;
    :)
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;

	h)
		echo "options are:
		-t for the number of threads to use

		"
		;;
  esac
done


# require minimum length for trimming
if [ -z "$trimGaloreLength" ]; then 
	echo "No minimum length given for trimming cutoff, exiting program"
	exit
fi

## if fastq.trimmed exists, delete it
if [ -d "fastq.trimmed" ]; then
  rm -rf fastq.trimmed
fi

## if fastx_trimexists, delete it
if [ -d "fastx_trim" ]; then
  rm -rf fastx_trim
fi



## if fastq.join exists, delete it
if [ -d "fastq.join" ]; then
  rm -rf fastq.join
fi


mkdir fastq.join
mkdir fastq.trimmed
mkdir fastx_trim
echo "" > trim_galore.out1 
echo "" > trim_galore.out2
echo "" > fastq-join.out1 
echo "" > fastq-join.out2

r1reads=($( ls fastq/ | grep 'R1' ))
r2reads=($( ls fastq/ | grep 'R2' ))

## first step is to clip and trim to remove CS1 and CS2 adaptors

## complete length of vector exon only product is 181bp, approx 90bp for exon1 and 91 for exon2
## therefore, shouldn't trim more than about 

for arrayposition in ${!r1reads[*]}

do
	echo " 5' trimming from ${r1reads[$arrayposition]} with fastX_trimmer"
	gunzip -c fastq/${r1reads[$arrayposition]} | fastx_trimmer -Q33 -f 24 -o fastx_trim/${r1reads[$arrayposition]%%.*}.fastx_trim.fastq
	echo " 5' trimming from ${r2reads[$arrayposition]} with fastX_trimmer"	
	gunzip -c fastq/${r2reads[$arrayposition]} | fastx_trimmer -Q33 -f 25 -o fastx_trim/${r2reads[$arrayposition]%%.*}.fastx_trim.fastq

	# join fastX-trimmed reads; default fastq-join settings are:
			# Options:
			# -o FIL          See 'Output' below
			# -v C            Verifies that the 2 files probe id's match up to char C
			#                   use '/' for Illumina reads
			# -p N            N-percent maximum difference (20)
			# -m N            N-minimum overlap (6)
			# -r FIL          Verbose stitch length report
#	echo "fastq-join on ${r1reads[$arrayposition]%%.*} ${r2reads[$arrayposition]%%.*} "		

	echo "pear to join reads on ${r1reads[$arrayposition]%%.*} ${r2reads[$arrayposition]%%.*} "		
	printf "${r1reads[$arrayposition]%%.*}\t${r2reads[$arrayposition]%%.*} \n\n" >> fastq-join.out1
	printf "${r1reads[$arrayposition]%%.*}\t${r2reads[$arrayposition]%%.*} \n\n" >> fastq-join.out2

	pear -f fastx_trim/${r1reads[$arrayposition]%%.*}.fastx_trim.fastq -r fastx_trim/${r2reads[$arrayposition]%%.*}.fastx_trim.fastq -v 6 -t 130 -n 130 -j $numbOfCores -o fastq.join/${r1reads[$arrayposition]%%_*}.join 1>>fastq-join.out1 2>> fastq-join.out2
	
	# -t minimum length of reads after trimming the low quality part
	# -j minimum number of threads to use	
	# -n minimum length of assembled sequences

	## will remove any reads less than cutoff, will remove 5bp sequence from 5' end of read 1, and 5 bp sequence from 5' end of read 2

	#after grepping reads the adaptor I should be removing is: r1 rc  cs1  AGACCAAGTCTCTGCTACCGTA    r2 rc  cs2  TGTAGAACCATGTCGTCAGTGT
	trim_galore --length $trimGaloreLength -a AGACCAAGTCTCTGCTACCGTA -a2 TGTAGAACCATGTCGTCAGTGT --clip_R1 24 --clip_R2 25 --dont_gzip --paired fastq/${r1reads[$arrayposition]} fastq/${r2reads[$arrayposition]} -o fastq.trimmed/ 1>>trim_galore.out1 2>> trim_galore.out2	

	
	echo "trim_galore on ${r1reads[$arrayposition]%%.*} ${r2reads[$arrayposition]%%.*} "
	printf "read pair\t${r1reads[$arrayposition]%%.*}\t${r2reads[$arrayposition]%%.*} \n\n " >> trim_galore.out2
	
done
