##Author: Darren Korbie

##################### Expected folders and files ###################################################
## [fastq.trimmed] - folder with trim_galore trimmed fastq files
##		- currently expects there to be paired-end reads
##
##	[well.info] - tab delimited text file with the following information
##		1			2		3		4			5		6				7					8	
##		fastqfile	well	type	Unspliced	cDNA	ExonicSequence	intron1_included	intron2_included
##
##
###################### strings used
##
##	well.info
##

while getopts ":l:t:h" opt; do
  case $opt in
	l)
		re='^[0-9]+$'
		if ! [[ $OPTARG =~ $re ]] ; then
			echo "error: Not a number, option -l requires a trim length" >&2; exit 1
		fi
		echo "-l was triggered for trim_galore, Parameter: $OPTARG" >&2
		trimGaloreLength=$OPTARG
		;;
	t)
		re='^[0-9]+$'
		if ! [[ $OPTARG =~ $re ]] ; then
			echo "error: Not a number, option -t requires a number to specify number of threads to use" >&2; exit 1
		fi
		echo "-t was triggered for the nubmer of cores/threads to utilize" >&2
		numbOfCores=$OPTARG
		;;

   \?)
		echo "Invalid option: -$OPTARG" >&2
		exit 1
		;;
    :)
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;

	h)
		echo "options are:
		-t for the number of threads to use

		"
		;;
  esac
done


## sequence of exon skipping sequence in pSPL3 based on vector exons 1 and 2
#original vector exon skip sequences
#exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGTGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAATTCACTCCTCAGGTGCAGGCTGCCTATCAGAAGGTGGTGGCTGGTGTGGCCAATGCCCTGGCTCACAAATACCACTGAGAT"
## modified  exon skipping sequence in pSPL3 based on vector exons 1 and 2 with 2bp deletion
#exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAATTCACTCCTCAGGTGCAGGCTGCCTATCAGAAGGTGGTGGCTGGTGTGGCCAATGCCCTGGCTCACAAATACCACTGAGAT"
exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAAT"

## if results.trinity exists, delete it
if [ -d "results.trinity" ]; then
  rm -rf results.trinity
fi




#mkdir results.contamination
mkdir results.trinity
printf "" > trinity.out1
printf "" > trinity.out2


echo "launching genome guided trinity splicing analysis"

##  read each paired fastq file into its own string in a results.trinity/ folder
joinreads=($(  ls fastq.join/ | grep 'join.assembled.fastq' ))
r1reads=($(  ls fastq.trimmed/ | grep 'R1.*fq$' ))
r2reads=($(  ls fastq.trimmed/ | grep 'R2.*fq$' ))


###################################################################################
## associate fluidigm barcode number with the well position in the original plate
##
##	[well.info] - tab delimited text file with the following information
##		1			2		3		4			5		6				7					8	
##		fastqfile	well	type	Unspliced	cDNA	ExonicSequence	intron1_included	intron2_included

for arrayposition in ${!r1reads[*]}

do
	###################### awkline code description #######################################
	## code that identifies line in well.info file that corresponds to the well of interest
	#	awkline=$(grep "${r1reads[$arrayposition]%%_*}" well.info)
	# to print out a specific field from $awkline use 
	#		$(echo "$awkline" | awk '{print $1}' )
	
	awkline=$(grep "${r1reads[$arrayposition]%%_*}" well.info)
		
	echo "processing  ${r1reads[$arrayposition]} and  ${r2reads[$arrayposition]} reads with Trinity
	well.info file correlates $(echo "$awkline" | awk '{print $1}' ) with $(echo "$awkline" | awk '{print $2}' )
	output folder is results.trinity/${r1reads[$arrayposition]%%_*}_$(echo "$awkline" | awk '{print $2}')"

	printf "*******************processing  ${r1reads[$arrayposition]} and  ${r2reads[$arrayposition]} reads with Trinity
	well.info file correlates $(echo "$awkline" | awk '{print $1}' ) with $(echo "$awkline" | awk '{print $2}' )
	output folder is results.trinity/${r1reads[$arrayposition]%%_*}_$(echo "$awkline" | awk '{print $2}' ) \n\n" >> trinity.out1
	
	printf "*******************processing  ${r1reads[$arrayposition]} and  ${r2reads[$arrayposition]} reads with Trinity
	well.info file correlates $(echo "$awkline" | awk '{print $1}' ) with $(echo "$awkline" | awk '{print $2}' )
	output folder is results.trinity/${r1reads[$arrayposition]%%_*}_$(echo "$awkline" | awk '{print $2}' ) \n\n" >> trinity.out2

	## next step is to assemble using Trinity
	## will read each well into its own folder in a results.trinity/ folder

	# read folder name into variable so I don't need to call awk all the time
	folder=(${r1reads[$arrayposition]%%_*}_$(echo "$awkline" | awk '{print $2}' ))
	exonType=($(echo "$awkline" | awk '{print $3}' ))
	barcode=($(echo "$awkline" | awk '{print $1}' ))
	
	echo results.trinity/"$folder"_trinity/
	
	# dynamic kmer cutoff based on total mapped reads
	kmer_cutoff=($(  samtools view -F 4 -c  results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam ))
	kmer_cutoff_pc=$(echo "(($kmer_cutoff*0.01)+0.5)/1" | bc)
	
	
	echo "	
	hello $kmer_cutoff, $kmer_cutoff_pc	
	"
	
	if [ $kmer_cutoff_pc -lt 100 ]; then
		echo "kmer cutoff is too low, readjusted to 100 read and kmer cutoff"
		kmer_cutoff_pc=100
	fi

	 
	#trinityrnaseq-Trinity-v2.3.2 --genome_guided_min_coverage 200 -min_glue 200 --genome_guided_min_reads_per_partition 1000
	Trinity --min_kmer_cov $kmer_cutoff_pc --KMER_SIZE 25 --genome_guided_bam results.gsnapMapping/$folder/$folder.$exonType.genomeguided.filtered.sorted.bam --min_contig_length 132 --SS_lib_type F    --genome_guided_max_intron 299  --no_version_check  --max_memory 10G --output results.trinity/"$folder"_trinity/ --CPU $numbOfCores --full_cleanup --max_internal_gap_same_path 2

	cat results.trinity/"$folder"_trinity/Trinity-GG.fasta
	
	echo "
	
	kmer cutoff set at $kmer_cutoff_pc
	
	"
	
done
