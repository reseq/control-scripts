##Author: Darren Korbie

##################### Expected folders and files ###################################################
## [fastq.trimmed] - folder with trim_galore trimmed fastq files
##		- currently expects there to be paired-end reads
##
##	[well.info] - tab delimited text file with the following information
##		1			2		3		4			5		6				7					8	
##		fastqfile	well	type	Unspliced	cDNA	ExonicSequence	intron1_included	intron2_included
##
##
###################### strings used
##
##	well.info
##

while getopts ":l:t:h" opt; do
  case $opt in
	t)
		re='^[0-9]+$'
		if ! [[ $OPTARG =~ $re ]] ; then
			echo "error: Not a number, option -t requires a number to specify number of threads to use" >&2; exit 1
		fi
		echo "-t was triggered for the nubmer of cores/threads to utilize" >&2
		numbOfCores=$OPTARG
		;;
   \?)
		echo "Invalid option: -$OPTARG" >&2
		exit 1
		;;
    :)
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;

	h)
		echo "options are:
		-t for the number of threads to use

		"
		;;
  esac
done


## sequence of exon skipping sequence in pSPL3 based on vector exons 1 and 2
#original vector exon skip sequences
#exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGTGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAATTCACTCCTCAGGTGCAGGCTGCCTATCAGAAGGTGGTGGCTGGTGTGGCCAATGCCCTGGCTCACAAATACCACTGAGAT"
## modified  exon skipping sequence in pSPL3 based on vector exons 1 and 2 with 2bp deletion
#exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAATTCACTCCTCAGGTGCAGGCTGCCTATCAGAAGGTGGTGGCTGGTGTGGCCAATGCCCTGGCTCACAAATACCACTGAGAT"
#using current pslp3 primers, exon skip product would be 181bp long (i.e., sequence below is exact amplicon sequence)
exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAAT"

## if results.gsnapMapping exists, delete it

if [ -d "results.gsnapMapping" ]; then
  rm -rf results.gsnapMapping
fi


mkdir results.gsnapMapping

printf "" > gsnapMapping.summary.txt
printf "" > gsnap.out1
printf "" > gsnap.out2

echo "launching genome guided trinity splicing analysis"

##  read each paired fastq file into its own string in a results.trinity/ folder
joinreads=($(  ls fastq.join/ | grep 'join.assembled.fastq' ))
r1reads=($(  ls fastq.trimmed/ | grep 'R1.*fq$' ))
r2reads=($(  ls fastq.trimmed/ | grep 'R2.*fq$' ))

#v8 filtering addition

filter1="B1ex10WT B1ex12WT B1ex18WT B1ex19WT B1ex20WT B1ex22WT B1ex23WT B1ex3WT B1ex5WT B1ex6WT B1ex8WT B1ex9WT B2ex12WT B2ex18WT B2ex2WT B2ex4WT B2ex5WT B2ex6WT B2ex7WT B2ex8WT B2ex9WT"


###################################################################################
## associate fluidigm barcode number with the well position in the original plate
##
##	[well.info] - tab delimited text file with the following information
##		1			2		3		4			5		6				7					8	
##		fastqfile	well	type	Unspliced	cDNA	ExonicSequence	intron1_included	intron2_included

for arrayposition in ${!r1reads[*]}

do
	###################### awkline code description #######################################
	## code that identifies line in well.info file that corresponds to the well of interest
	#	awkline=$(grep "${r1reads[$arrayposition]%%_*}" well.info)
	# to print out a specific field from $awkline use 
	#		$(echo "$awkline" | awk '{print $1}' )
	
	awkline=$(grep "${r1reads[$arrayposition]%%_*}" well.info)


	# read folder name into variable so I don't need to call awk all the time
	folder=(${r1reads[$arrayposition]%%_*}_$(echo "$awkline" | awk '{print $2}' ))
	exonType=($(echo "$awkline" | awk '{print $3}' ))
	barcode=($(echo "$awkline" | awk '{print $1}' ))
	
	echo "processing  ${r1reads[$arrayposition]} and  ${r2reads[$arrayposition]} reads with gmap
	well.info file correlates $(echo "$awkline" | awk '{print $1}' ) with $(echo "$awkline" | awk '{print $2}' )
	output folder is rresults.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam"

	printf "*******************processing  ${r1reads[$arrayposition]} and  ${r2reads[$arrayposition]} reads with gmap
	well.info file correlates $(echo "$awkline" | awk '{print $1}' ) with $(echo "$awkline" | awk '{print $2}' )
	output folder is rresults.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam \n\n" >> gsnap.out1
	
	printf "*******************processing  ${r1reads[$arrayposition]} and  ${r2reads[$arrayposition]} reads with Trinity
	well.info file correlates $(echo "$awkline" | awk '{print $1}' ) with $(echo "$awkline" | awk '{print $2}' )
	output folder is rresults.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam \n\n" >> gsnap.out2

	## next step is to assemble using Trinity
	## will read each well into its own folder in a results.trinity/ folder
	
	mkdir results.gsnapMapping/$folder/


	#############	genome-guided trinity using blat
	#############	
	
	#samtools index results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.gmap.bam
	
	#trying gsnap to improve trimming problems gsnap  --min-coverage 0.8  --quality-protocol=illumina  --maxsearch 25 -k 16 
	  # -y, --max-middle-insertions=INT  Maximum number of middle insertions allowed (default 9)
	  # -z, --max-middle-deletions=INT Maximum number of middle deletions allowed (default 30)
	  # -Y, --max-end-insertions=INT   Maximum number of end insertions allowed (default 3)
	  # -Z, --max-end-deletions=INT    Maximum number of end deletions allowed (default 6)	
	  # --expand-offsets=INT           Whether to expand the genomic offsets index
                                   # Values: 0 (no, default), or 1 (yes).
                                   # Expansion gives faster alignment, but requires more memory
	#gsnap 	-d decgsnap.2717-032.brca1_2.wt_reference.unspliced fastq.join/$barcode.join.assembled.fastq  -A sam  --npaths 1 -N 1 -t 14 -w 299 --nofails --expand-offsets=1 -B 5 --gmap-mode=terminal --use-shared-memory=0 | samtools view -bS - | samtools sort -  results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted	

	#############	mapping using bowtie2.3 and histat
	#############	mapping_indexes/reference_unspliced
	
	hisat2 -x mapping_indexes/reference_unspliced -U fastq.join/$barcode.join.assembled.fastq -S hisstat_out.sam  --pen-noncansplice 8 --max-intronlen 299 --no-unal  -p $numbOfCores --no-softclip --score-min L,0,-0.2  --un unaliged_reads.fastq	
	
	samtools view -bS hisstat_out.sam | samtools sort -  results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted
	
	samtools index results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam

	# create a filtered BAM which has the cryptic exon and exon skipping reads removed, to improve Trinity mapping
	# -R FILE Output alignments in read groups listed in FILE [null]. 
	
	
	samtools view -b -L filter.txt results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam   | samtools sort - results.gsnapMapping/$folder/$folder.$exonType.genomeguided.filtered.sorted

	
	
	samtools index results.gsnapMapping/$folder/$folder.$exonType.genomeguided.filtered.sorted.bam
	
	echo "
	
	"
	echo "total reads is "  
	echo $(wc -l fastq.join/$barcode.join.assembled.fastq | cut -d ' ' -f1)/4 | bc
	
	echo "mapped gnsap reads is"	
	samtools view -F 4 -c  results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam
	
	
	
	

	printf "$barcode \n$( echo $(wc -l fastq.join/$barcode.join.assembled.fastq | cut -d ' ' -f1)/4 | bc ) total reads \n$( samtools view -F 4 -c results.gsnapMapping/$folder/$folder.$exonType.genomeguided.sorted.bam) mapped gnsap reads \n" >> gsnapMapping.summary.txt

	rm hisstat_out.sam
	
done
