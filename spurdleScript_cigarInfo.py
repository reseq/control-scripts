#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 29 20:55:10 2014

@author: Darren Korbie
"""
'''
This is a script to read the data generated for the spurdle project into a single file

File columns for output

0	Plate
1	FLD
2	well
3	well letter
4	well number
5	Brca exon
6	splice type
7	exon size
8	intron1 size
9	intron2 size
10	trim galore result
11	join result?

'''
######################################
######################################
######################################

def barcodeFill(number):
    id=str(number)    
    if (len(id) == 1):
        barcode='FLD000'+ id
    elif (len(id) == 2):
        barcode='FLD00' + id
    elif (len(id) == 3):
        barcode='FLD0' + id
    return(barcode)

######################################
######################################
######################################

plateInfoFile=open('plateinfo.txt', 'r')

plateinfo_Line=plateInfoFile.readline().rstrip()
plateInfoFile.close()

import csv     
import glob
# read Sequences file provided by Spurdle lab 
for file in glob.glob('./*transfections.txt'):
    sequences_File = open(file, 'r')
    reader = csv.reader(sequences_File, delimiter='\t')
    SequencesFile_List = list(reader)

'''
Is expected that all transfection.txt Files will have this structure:
0	Transfection Dates
1	Position
2	Gene
3	Exon
4	Variant
5	Prediction
6	Barcode
7	Vector Exon 1
8	BRCA Intron 1
9	BRCA Exon
10	BRCA Intron 2
11	Vector Exon 2



'''

## each bit of file info will be read into a python dictionary, using the fluidgim barcode as the key
## break the code into segments, each segment processing one set of fields and one file at a time

# generate a dictionary with all FLD barcodes as keys

import re
datasummary_Dictionary={}
#headers for dictionary
datasummary_Dictionary['FLD0000']=['Plate',
'FLD',
'well',
'well letter',
'well number',
'Brca exon',
'splice type',
'prediction',
'exon size',
'intron1 size',
'intron2 size',
'trimG total reads',
'trimG pairs removed',
'trimG % removed',
'bt2 total reads',
'bt2 overall %',
'join total reads',
'total joined', 
'join %',
'gmap total reads', 
'gmap mapped reads',
'% map gmap reads']
entriesInSequencesFileList=len(SequencesFile_List)
SequencesFileListPosition=1
while (SequencesFileListPosition < entriesInSequencesFileList ):
    # extract barcode, generate proper FLDXXXX as key for dictionary
    datasummary_Dictionary[barcodeFill(SequencesFile_List[SequencesFileListPosition][6])]=[
    #add plate ID
    plateinfo_Line,
    #add FLD ID
    barcodeFill(SequencesFile_List[SequencesFileListPosition][6]),    
    #add well ID, e.g., C12
    SequencesFile_List[SequencesFileListPosition][1],
    #add well letter, e.g., C
    SequencesFile_List[SequencesFileListPosition][1][0:1],
    #add well number, e.g., 4
    int(SequencesFile_List[SequencesFileListPosition][1][1:]),    
    #add BRCA exon, e.g., B1ex4
    SequencesFile_List[SequencesFileListPosition][2] + SequencesFile_List[SequencesFileListPosition][3],    
    #add splice type, e.g., c.4097-7A-T    
    SequencesFile_List[SequencesFileListPosition][4],
    #add prediction
    SequencesFile_List[SequencesFileListPosition][5],     
    #add exon size
    len(SequencesFile_List[SequencesFileListPosition][9]),      
    #add intron1 size
    len(SequencesFile_List[SequencesFileListPosition][8]),      
    #add intron2 size
    len(SequencesFile_List[SequencesFileListPosition][10])          
    ]  
    SequencesFileListPosition += 1




'''
add trim galore result
'''

trimgalore_File=open('trim_galore.out2', 'r')
for trimGaloreLine in trimgalore_File:
    if re.search(r'^Total number of sequences analysed', trimGaloreLine):
        splitLine=trimGaloreLine.split(": ")
        totalreads = int(splitLine[1].rstrip())
    #TrimGaloreReads
    elif re.search(r'^Number of sequence pairs removed', trimGaloreLine):
        splitLine=re.split(r"\)|\(| ", trimGaloreLine)
        pairs =  int(splitLine[20])
        percentage = splitLine[22]       
    #ReadPairsRemoved and % Removed
    elif re.search(r'^Deleting both intermediate output files', trimGaloreLine):
        splitLine=re.split(r" |_", trimGaloreLine)
        fldID=splitLine[5]
        datasummary_Dictionary[fldID].append(totalreads)
        datasummary_Dictionary[fldID].append(pairs)
        datasummary_Dictionary[fldID].append(percentage)
    if not trimGaloreLine:
        break



'''
add bowtie2 quantitative mapping results
'''

bowtie2_File=open('bowtie2.out2', 'r')
for each_Line in bowtie2_File:
    # fld ID
    if re.search(r'^\*', each_Line):
        splitLine=re.split(r"/|_", each_Line)
        fldID = splitLine[13]
    # total reads analyzed
    elif re.search(r'reads', each_Line):
        splitLine=re.split(r" ", each_Line)
        totalReads=int(splitLine[0])
    elif re.search(r'overall', each_Line):
        splitLine=re.split(r" ", each_Line)
        mappingPercent=splitLine[0]
        datasummary_Dictionary[fldID].append(totalReads)
        datasummary_Dictionary[fldID].append(mappingPercent)
    if not each_Line:
        break

bowtie2_File.close()



'''
 add fastq-join results
'''
join_File=open('fastq-join.out1', 'r')
for each_Line in join_File:
    # fld ID
    if re.search(r'^FLD', each_Line):
        splitLine=re.split(r"_", each_Line)
        fldID = splitLine[0]
    # total reads analyzed
    # total joined
    # percent joined        
    elif re.search(r'Assembled reads \.', each_Line):
        splitLine=re.split(r" ", each_Line)
        totaljoin=int(re.sub(r',', '', splitLine[3]))
        totalReads=int(re.sub(r',', '', splitLine[5]))
        percentJoin=float(splitLine[6][1:-3])
        datasummary_Dictionary[fldID].append(totalReads)
        datasummary_Dictionary[fldID].append(totaljoin)
        datasummary_Dictionary[fldID].append(percentJoin)
    if not each_Line:
        break


join_File.close()


'''
 add gmap results results
'''
gmap_File=open('gsnapMapping.summary.txt', 'r')
loop1=0
for each_Line in gmap_File:
    # fld ID
    if re.search(r'^FLD', each_Line):
        splitLine=re.split(r"_", each_Line)
        fldID = splitLine[0].rstrip()
    # total reads analyzed
    # total joined
    # percent joined        
    elif re.search(r'^[0-9]', each_Line):
        splitLine=re.split(r" ", each_Line)
        total_reads = splitLine[0]
        nextline2 = next(gmap_File)        
        splitLine=re.split(r" ", nextline2)
        mapped_reads = splitLine[0]
        if int(total_reads) != 0:
            overall_map_pc = round((int(mapped_reads)/int(total_reads)*100), 2)
        else: overall_map_pc = 0
        datasummary_Dictionary[fldID].append(total_reads)
        datasummary_Dictionary[fldID].append(mapped_reads) 
        datasummary_Dictionary[fldID].append(overall_map_pc) 

    if not each_Line:
        break


gmap_File.close()

'''
add assembly results
will read in using summary.coverage_and_cigarValues.tab.txt
'''

cigarResults_List=[]
#generate headers
cigarResults_List=[[
    'Plate',
    'FLD',
    'well',
    'well letter',
    'well number',
    'Brca exon',
    'splice type',
    'prediction',
    'transcriptID', 
    '% of libary',
    'cigar string',
    'start',
    'stop',
    'reads',
    'bases covered',
    'contaminated with', 
    'contamination call']]
cigar_File=open('summary.coverage_and_cigarValues.tab.txt', 'r')
for each_Line in cigar_File:
    '''
    splitline info
    0 - transcript id/chrom
    1 - start
    2- stop
    3 - reads
    4 - bases covered
    5 - % of library
    6 - mapped/unmapped against cDNA
    7 - cigar string of transcript
    '''
    # fld ID
    if re.search(r'^results', each_Line):
        splitLine=re.split(r"/|_", each_Line)
        fldID = splitLine[3]
    #phiX
    elif re.search(r'^NC_001422', each_Line):
        splitLine=re.split(r"\t", each_Line)
        results=[]
        for eachEntry in datasummary_Dictionary[fldID][0:8]:
            results.append(eachEntry)
        results.extend([
        splitLine[0], 
        float(splitLine[5]), 
        'na', 
        splitLine[1], 
        int(splitLine[2]), 
        int(splitLine[3]), 
        splitLine[4]])
        cigarResults_List.append(results)
    #exonskipping
    elif re.search(r'^exon_skipping', each_Line):
        splitLine=re.split(r"\t", each_Line)
        results=[]
        for eachEntry in datasummary_Dictionary[fldID][0:8]:
            results.append(eachEntry)
        results.extend([
        splitLine[0], 
        float(splitLine[5]), 
        'na', 
        splitLine[1], 
        int(splitLine[2]), 
        int(splitLine[3]), 
        splitLine[4]])
        cigarResults_List.append(results)
    #assemblies
    elif re.search(r"^c0|^TR", each_Line):
        splitLine=re.split(r"\t", each_Line)
        results=[]
        try:
            cigar = splitLine[7]
        except IndexError:
            cigar = 'na'
        for eachEntry in datasummary_Dictionary[fldID][0:8]:
            results.append(eachEntry)
            
 
        results.extend([
        splitLine[0], 
        float(splitLine[5]), 
        cigar,
        splitLine[1], 
        int(splitLine[2]), 
        int(splitLine[3]), 
        splitLine[4]])
        cigarResults_List.append(results)
    else:
        continue


'''
third output format
reading cigar values into a single line for easy comparison
'''

cigar_File=open('summary.coverage_and_cigarValues.tab.txt', 'r')
cigarResults_List2=[[
    'Plate',
    'FLD',
    'well',
    'well letter',
    'well number',
    'Brca exon',
    'splice type',
    'prediction',
    'Cigar string']]

cigarResults=[]
lineInfo=[]

for each_Line in cigar_File:

    # fld ID
    if re.search(r'^results', each_Line):
        #cigarResults.sort()
        cigarResultsSorted=sorted(cigarResults, reverse=True)
        lineInfo.extend(cigarResultsSorted)        
        cigarResults_List2.append(lineInfo)
        cigarResults=[]
        lineInfo=[]       
        splitLine=re.split(r"/|_", each_Line)
        fldID = splitLine[3]
        for eachEntry in datasummary_Dictionary[fldID][0:8]:
            lineInfo.append(eachEntry)
    #assemblies
    elif re.search(r"^c0|^TR", each_Line):
        splitLine=re.split(r"\t", each_Line)
        try:
            cigarvalue = splitLine[7]
        except IndexError:
            cigarvalue = 'na'
        cigarResults.append(cigarvalue)
    else:
        continue

cigarResultsSorted=sorted(cigarResults, reverse=True)
lineInfo.extend(cigarResultsSorted)        
cigarResults_List2.append(lineInfo)
        
        
cigar_File.close()



#################
#
# contamination script
#
#
################

contamination_Dictionary={}
#headers for dictionary
contamination_Dictionary['FLD0000']=['FLD',
'well',
'wellLetter',
'WellNumber',
'ExpectedExon',
'DetectedExon',
'Contaminated_With',
'trinityTranscript',
'cigarmatch'
]
# read in contamination report

for LINE in open('summary.contamination.tab.txt'):

    if re.search(r'^F', LINE):
        splitLine=LINE.split("_")
        fldID=splitLine[0]
        well=splitLine[1]
        contamination_Dictionary[fldID]=[fldID]
        contamination_Dictionary[fldID].append(well) 
        contamination_Dictionary[fldID].append(well[0]) 
        contamination_Dictionary[fldID].append(int(well[1:]) )
    #determine expected exon
    elif re.search(r'^expect', LINE):
        splitLine=re.split(r"\t", LINE)
        expectedExon=splitLine[1].rstrip()
        contamination_Dictionary[fldID].append(expectedExon)
    #determine if exons are as expected
    elif re.search(r'^B', LINE):
        splitLine=re.split(r"\t", LINE)
        exonMatch=splitLine[0][:-2]
        trinityTranscript=splitLine[2]
        cigarMatch=splitLine[5]
        if (expectedExon != exonMatch):
            contamination_Dictionary[fldID].append('contamination')
    # also append contamination outcome check to cigarResults_List            
            loop=0
            for sublist in cigarResults_List:
                if (sublist[1] == fldID and sublist[8] == trinityTranscript):
                    cigarResults_List[loop].append(exonMatch)  				
                    cigarResults_List[loop].append('contamination')
                    break
                else:
                    loop+=1
        else:
            contamination_Dictionary[fldID].append(exonMatch)		
        contamination_Dictionary[fldID].append(' ') 
        contamination_Dictionary[fldID].append(trinityTranscript)
        contamination_Dictionary[fldID].append(cigarMatch)        
    # Prints: Found it! ['a', 'c']
    #section to deal with cryptic exon calls
    #added in v8
    elif re.search(r'^cryptic', LINE):
        splitLine=re.split(r"\t", LINE)
        exonMatch=splitLine[0]
        trinityTranscript=splitLine[2]
        cigarMatch=splitLine[5]
        contamination_Dictionary[fldID].append(exonMatch)		
        contamination_Dictionary[fldID].append(' ')


        contamination_Dictionary[fldID].append(trinityTranscript)
        contamination_Dictionary[fldID].append(cigarMatch)        			





#write results to excel file

import xlwt



workbook = xlwt.Workbook()
sheet1 = workbook.add_sheet('summary')
sheet2 = workbook.add_sheet('cigar1')
sheet3 = workbook.add_sheet('cigar2')
#have removed the contamination sheet, since this info is capture in a more readable from the cigar1 output
row=0

# Worksheet.write(row_index,column_index,value)
keys=list(datasummary_Dictionary.keys())
keys.sort()
for eachKey in keys:
    for c, eachEntry in enumerate(datasummary_Dictionary[eachKey]):
        sheet1.write(row, c, eachEntry)
    row += 1
    
for a, b in enumerate(cigarResults_List):
    for c, col in enumerate(b):
        sheet2.write(a, c, col)  

for a, b in enumerate(cigarResults_List2):
    for c, col in enumerate(b):
        sheet3.write(a, c, col)  

workbook.save(plateinfo_Line + '.xls')
