##Author: Darren Korbie
# ##########################################################################################################################
# ##############################		transfer all files into a summary folder  						     ###############
# ##########################################################################################################################	

## script expects command line entry to title zip archive
rm -rf results.final_summary
mkdir results.final_summary
mkdir results.final_summary/referenceFasta

resultsfolderBt2=($(  ls results.bowtie2_mapping ))
resultsfolderIGV=($(  ls results.bowtie2_mapping_igv ))
resultsfolderTrinity=($( ls results.trinity ))
resultsfolderGSNAP=($( ls results.gsnapMapping ))

# copy STDOUT and STDERROR files in archive, and well.info

rm -rf results.final_summary/info
mkdir results.final_summary/info
	cp *out* results.final_summary/info/
	cp well.info results.final_summary/info/
	cp summary* results.final_summary/info/
	cp mapping_indexes/reference_unspliced.fa results.final_summary/referenceFasta
	cp mapping_indexes/reference_exon.fa results.final_summary/referenceFasta
	cp *transfections.txt results.final_summary/info/
	cp *xls results.final_summary/info/
# copy primary fastq/ folder into results.final_summary for archive purposes
	

for arrayposition in ${!resultsfolderBt2[*]}

# want to copy from results.bowtie2_mapping
#
#	trinity*.fasta
#	*sort.bam
#	*sort.bam.bai
#want to copy from results.bowtie2_mapping_igv
#	reads.*.sort.bam/bai
#	reference_construc.*.sort.bam/bai
#	reference_trinity.*.sort.bam/bai
#	*.fasta
#want to copy from results.gsnapMapping/barcode
#	*sorted.bam


	do
		mkdir results.final_summary/${resultsfolderBt2[$arrayposition]}

		# copy from results.bowtie2_mapping
		mkdir results.final_summary/${resultsfolderBt2[$arrayposition]}/bt2_quant
		cp results.bowtie2_mapping/${resultsfolderBt2[$arrayposition]}/${resultsfolderBt2[$arrayposition]}.fasta results.final_summary/${resultsfolderBt2[$arrayposition]}/bt2_quant/
		cp results.bowtie2_mapping/${resultsfolderBt2[$arrayposition]}/${resultsfolderBt2[$arrayposition]}.sort.bam* results.final_summary/${resultsfolderBt2[$arrayposition]}/bt2_quant/

		# copy from results.bowtie2_mapping_igv
		cp results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/*sort.bam* results.final_summary/${resultsfolderBt2[$arrayposition]}/
		cp results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/${resultsfolderBt2[$arrayposition]}.fasta  results.final_summary/${resultsfolderBt2[$arrayposition]}/
	done

	# copy from results.gsnapMapping
	# bowtie folders and gsnap folders have slightly different name, was easier to write a separate loop to copy them

for arrayposition in ${!resultsfolderGSNAP[*]}
	 do
		 mkdir results.final_summary/${resultsfolderBt2[$arrayposition]}/genomeGuidedBam/
		 cp results.gsnapMapping/${resultsfolderGSNAP[$arrayposition]}/*sorted.bam results.final_summary/${resultsfolderGSNAP[$arrayposition]}_trinity/genomeGuidedBam
		 cp results.gsnapMapping/${resultsfolderGSNAP[$arrayposition]}/*sorted.bam.bai results.final_summary/${resultsfolderGSNAP[$arrayposition]}_trinity/genomeGuidedBam		
	done
