##Author: Darren Korbie

## files needed
#		[well.info] - tab delimited text file with the following information
#		1			2		3		4			5		6				7					8	
#		fastqfile	well	type	Unspliced	cDNA	ExonicSequence	intron1_included	intron2_included
#
#	

## to be able to include blanks in the bowtie2 mapping bash needs to have the IFS set without spaces.  
## Critical: in the well_construct_cdna.fa file, it's necessary to have blank space for the fasta sequence	


while getopts ":l:p:t:h" opt; do
  case $opt in
	t)
		re='^[0-9]+$'
		if ! [[ $OPTARG =~ $re ]] ; then
			echo "error: Not a number, option -t requires a number to specify number of threads to use" >&2; exit 1
		fi
		echo "-t was triggered for the nubmer of cores/threads to utilize" >&2
		numbOfCores=$OPTARG
		;;
   \?)
		echo "Invalid option: -$OPTARG" >&2
		exit 1
		;;
    :)
		echo "Option -$OPTARG requires an argument." >&2
		exit 1
		;;

	h)
		echo "options are:
		-t for the number of threads to use

		"
		;;
  esac
done

$numbofCores



SAVEIFS=$IFS
IFS=$(echo -en "\n\b")


if [ -d "results.contamination" ]; then
  rm -rf results.contamination
fi


rm -rf results.bowtie2_mapping
rm -rf results.bowtie2_mapping_igv
mkdir results.bowtie2_mapping
mkdir results.bowtie2_mapping_igv
mkdir results.contamination
printf "" > bowtie2.out1
printf "" > bowtie2.out2
printf "" > bowtie2_igv.out1
printf "" > bowtie2_igv.out2

## sequence of pSPL3 vector exons 1 and 2
# original vector exon sequences
#vectorexons=">vector_exon1\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGTGCTCTAGAGTCGACCCAGCA\n>vector_exon2\nACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAAT"
## modified for deletion in vector exon 1
vectorexons=">vector_exon1\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCA\n>vector_exon2\nACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAAT"

## sequence of exon skipping sequence in pSPL3 based on vector exons 1 and 2
#original vector exon skip sequences
#exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGTGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAATTCACTCCTCAGGTGCAGGCTGCCTATCAGAAGGTGGTGGCTGGTGTGGCCAATGCCCTGGCTCACAAATACCACTGAGAT"
## modified  exon skipping sequence in pSPL3 based on vector exons 1 and 2 with 2bp deletion
#exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAATTCACTCCTCAGGTGCAGGCTGCCTATCAGAAGGTGGTGGCTGGTGTGGCCAATGCCCTGGCTCACAAATACCACTGAGAT"
# exonskip above is 261 bp, will redo with vector exons above which equals 180bp
exonskip=">exon_skipping\nTCTGAGTCACCTGGACAACCTCAAAGGCACCTTTGCTAAGCTGAGTGAACTGCACTGTGACAAGCTGCACGCTCTAGAGTCGACCCAGCAACCTGGAGATCTCCCGAGGGGACCCGACAGGCCCGAAGGAATAGAAGAAGAAGGTGGAGAGAGAGACAGAGACAGATCCATTTCGACCAAT"

phiX_reference=">NC_001422.1_phiX174
GAGTTTTATCGCTTCCATGACGCAGAAGTTAACACTTTCGGATATTTCTGATGAGTCGAAAAATTATCTT
GATAAAGCAGGAATTACTACTGCTTGTTTACGAATTAAATCGAAGTGGACTGCTGGCGGAAAATGAGAAA
ATTCGACCTATCCTTGCGCAGCTCGAGAAGCTCTTACTTTGCGACCTTTCGCCATCAACTAACGATTCTG
TCAAAAACTGACGCGTTGGATGAGGAGAAGTGGCTTAATATGCTTGGCACGTTCGTCAAGGACTGGTTTA
GATATGAGTCACATTTTGTTCATGGTAGAGATTCTCTTGTTGACATTTTAAAAGAGCGTGGATTACTATC
TGAGTCCGATGCTGTTCAACCACTAATAGGTAAGAAATCATGAGTCAAGTTACTGAACAATCCGTACGTT
TCCAGACCGCTTTGGCCTCTATTAAGCTCATTCAGGCTTCTGCCGTTTTGGATTTAACCGAAGATGATTT
CGATTTTCTGACGAGTAACAAAGTTTGGATTGCTACTGACCGCTCTCGTGCTCGTCGCTGCGTTGAGGCT
TGCGTTTATGGTACGCTGGACTTTGTGGGATACCCTCGCTTTCCTGCTCCTGTTGAGTTTATTGCTGCCG
TCATTGCTTATTATGTTCATCCCGTCAACATTCAAACGGCCTGTCTCATCATGGAAGGCGCTGAATTTAC
GGAAAACATTATTAATGGCGTCGAGCGTCCGGTTAAAGCCGCTGAATTGTTCGCGTTTACCTTGCGTGTA
CGCGCAGGAAACACTGACGTTCTTACTGACGCAGAAGAAAACGTGCGTCAAAAATTACGTGCGGAAGGAG
TGATGTAATGTCTAAAGGTAAAAAACGTTCTGGCGCTCGCCCTGGTCGTCCGCAGCCGTTGCGAGGTACT
AAAGGCAAGCGTAAAGGCGCTCGTCTTTGGTATGTAGGTGGTCAACAATTTTAATTGCAGGGGCTTCGGC
CCCTTACTTGAGGATAAATTATGTCTAATATTCAAACTGGCGCCGAGCGTATGCCGCATGACCTTTCCCA
TCTTGGCTTCCTTGCTGGTCAGATTGGTCGTCTTATTACCATTTCAACTACTCCGGTTATCGCTGGCGAC
TCCTTCGAGATGGACGCCGTTGGCGCTCTCCGTCTTTCTCCATTGCGTCGTGGCCTTGCTATTGACTCTA
CTGTAGACATTTTTACTTTTTATGTCCCTCATCGTCACGTTTATGGTGAACAGTGGATTAAGTTCATGAA
GGATGGTGTTAATGCCACTCCTCTCCCGACTGTTAACACTACTGGTTATATTGACCATGCCGCTTTTCTT
GGCACGATTAACCCTGATACCAATAAAATCCCTAAGCATTTGTTTCAGGGTTATTTGAATATCTATAACA
ACTATTTTAAAGCGCCGTGGATGCCTGACCGTACCGAGGCTAACCCTAATGAGCTTAATCAAGATGATGC
TCGTTATGGTTTCCGTTGCTGCCATCTCAAAAACATTTGGACTGCTCCGCTTCCTCCTGAGACTGAGCTT
TCTCGCCAAATGACGACTTCTACCACATCTATTGACATTATGGGTCTGCAAGCTGCTTATGCTAATTTGC
ATACTGACCAAGAACGTGATTACTTCATGCAGCGTTACCATGATGTTATTTCTTCATTTGGAGGTAAAAC
CTCTTATGACGCTGACAACCGTCCTTTACTTGTCATGCGCTCTAATCTCTGGGCATCTGGCTATGATGTT
GATGGAACTGACCAAACGTCGTTAGGCCAGTTTTCTGGTCGTGTTCAACAGACCTATAAACATTCTGTGC
CGCGTTTCTTTGTTCCTGAGCATGGCACTATGTTTACTCTTGCGCTTGTTCGTTTTCCGCCTACTGCGAC
TAAAGAGATTCAGTACCTTAACGCTAAAGGTGCTTTGACTTATACCGATATTGCTGGCGACCCTGTTTTG
TATGGCAACTTGCCGCCGCGTGAAATTTCTATGAAGGATGTTTTCCGTTCTGGTGATTCGTCTAAGAAGT
TTAAGATTGCTGAGGGTCAGTGGTATCGTTATGCGCCTTCGTATGTTTCTCCTGCTTATCACCTTCTTGA
AGGCTTCCCATTCATTCAGGAACCGCCTTCTGGTGATTTGCAAGAACGCGTACTTATTCGCCACCATGAT
TATGACCAGTGTTTCCAGTCCGTTCAGTTGTTGCAGTGGAATAGTCAGGTTAAATTTAATGTGACCGTTT
ATCGCAATCTGCCGACCACTCGCGATTCAATCATGACTTCGTGATAAAAGATTGAGTGTGAGGTTATAAC
GCCGAAGCGGTAAAAATTTTAATTTTTGCCGCTGAGGGGTTGACCAAGCGAAGCGCGGTAGGTTTTCTGC
TTAGGAGTTTAATCATGTTTCAGACTTTTATTTCTCGCCATAATTCAAACTTTTTTTCTGATAAGCTGGT
TCTCACTTCTGTTACTCCAGCTTCTTCGGCACCTGTTTTACAGACACCTAAAGCTACATCGTCAACGTTA
TATTTTGATAGTTTGACGGTTAATGCTGGTAATGGTGGTTTTCTTCATTGCATTCAGATGGATACATCTG
TCAACGCCGCTAATCAGGTTGTTTCTGTTGGTGCTGATATTGCTTTTGATGCCGACCCTAAATTTTTTGC
CTGTTTGGTTCGCTTTGAGTCTTCTTCGGTTCCGACTACCCTCCCGACTGCCTATGATGTTTATCCTTTG
AATGGTCGCCATGATGGTGGTTATTATACCGTCAAGGACTGTGTGACTATTGACGTCCTTCCCCGTACGC
CGGGCAATAACGTTTATGTTGGTTTCATGGTTTGGTCTAACTTTACCGCTACTAAATGCCGCGGATTGGT
TTCGCTGAATCAGGTTATTAAAGAGATTATTTGTCTCCAGCCACTTAAGTGAGGTGATTTATGTTTGGTG
CTATTGCTGGCGGTATTGCTTCTGCTCTTGCTGGTGGCGCCATGTCTAAATTGTTTGGAGGCGGTCAAAA
AGCCGCCTCCGGTGGCATTCAAGGTGATGTGCTTGCTACCGATAACAATACTGTAGGCATGGGTGATGCT
GGTATTAAATCTGCCATTCAAGGCTCTAATGTTCCTAACCCTGATGAGGCCGCCCCTAGTTTTGTTTCTG
GTGCTATGGCTAAAGCTGGTAAAGGACTTCTTGAAGGTACGTTGCAGGCTGGCACTTCTGCCGTTTCTGA
TAAGTTGCTTGATTTGGTTGGACTTGGTGGCAAGTCTGCCGCTGATAAAGGAAAGGATACTCGTGATTAT
CTTGCTGCTGCATTTCCTGAGCTTAATGCTTGGGAGCGTGCTGGTGCTGATGCTTCCTCTGCTGGTATGG
TTGACGCCGGATTTGAGAATCAAAAAGAGCTTACTAAAATGCAACTGGACAATCAGAAAGAGATTGCCGA
GATGCAAAATGAGACTCAAAAAGAGATTGCTGGCATTCAGTCGGCGACTTCACGCCAGAATACGAAAGAC
CAGGTATATGCACAAAATGAGATGCTTGCTTATCAACAGAAGGAGTCTACTGCTCGCGTTGCGTCTATTA
TGGAAAACACCAATCTTTCCAAGCAACAGCAGGTTTCCGAGATTATGCGCCAAATGCTTACTCAAGCTCA
AACGGCTGGTCAGTATTTTACCAATGACCAAATCAAAGAAATGACTCGCAAGGTTAGTGCTGAGGTTGAC
TTAGTTCATCAGCAAACGCAGAATCAGCGGTATGGCTCTTCTCATATTGGCGCTACTGCAAAGGATATTT
CTAATGTCGTCACTGATGCTGCTTCTGGTGTGGTTGATATTTTTCATGGTATTGATAAAGCTGTTGCCGA
TACTTGGAACAATTTCTGGAAAGACGGTAAAGCTGATGGTATTGGCTCTAATTTGTCTAGGAAATAACCG
TCAGGATTGACACCCTCCCAATTGTATGTTTTCATGCCTCCAAATCTTGGAGGCTTTTTTATGGTTCGTT
CTTATTACCCTTCTGAATGTCACGCTGATTATTTTGACTTTGAGCGTATCGAGGCTCTTAAACCTGCTAT
TGAGGCTTGTGGCATTTCTACTCTTTCTCAATCCCCAATGCTTGGCTTCCATAAGCAGATGGATAACCGC
ATCAAGCTCTTGGAAGAGATTCTGTCTTTTCGTATGCAGGGCGTTGAGTTCGATAATGGTGATATGTATG
TTGACGGCCATAAGGCTGCTTCTGACGTTCGTGATGAGTTTGTATCTGTTACTGAGAAGTTAATGGATGA
ATTGGCACAATGCTACAATGTGCTCCCCCAACTTGATATTAATAACACTATAGACCACCGCCCCGAAGGG
GACGAAAAATGGTTTTTAGAGAACGAGAAGACGGTTACGCAGTTTTGCCGCAAGCTGGCTGCTGAACGCC
CTCTTAAGGATATTCGCGATGAGTATAATTACCCCAAAAAGAAAGGTATTAAGGATGAGTGTTCAAGATT
GCTGGAGGCCTCCACTATGAAATCGCGTAGAGGCTTTGCTATTCAGCGTTTGATGAATGCAATGCGACAG
GCTCATGCTGATGGTTGGTTTATCGTTTTTGACACTCTCACGTTGGCTGACGACCGATTAGAGGCGTTTT
ATGATAATCCCAATGCTTTGCGTGACTATTTTCGTGATATTGGTCGTATGGTTCTTGCTGCCGAGGGTCG
CAAGGCTAATGATTCACACGCCGACTGCTATCAGTATTTTTGTGTGCCTGAGTATGGTACAGCTAATGGC
CGTCTTCATTTCCATGCGGTGCACTTTATGCGGACACTTCCTACAGGTAGCGTTGACCCTAATTTTGGTC
GTCGGGTACGCAATCGCCGCCAGTTAAATAGCTTGCAAAATACGTGGCCTTATGGTTACAGTATGCCCAT
CGCAGTTCGCTACACGCAGGACGCTTTTTCACGTTCTGGTTGGTTGTGGCCTGTTGATGCTAAAGGTGAG
CCGCTTAAAGCTACCAGTTATATGGCTGTTGGTTTCTATGTGGCTAAATACGTTAACAAAAAGTCAGATA
TGGACCTTGCTGCTAAAGGTCTAGGAGCTAAAGAATGGAACAACTCACTAAAAACCAAGCTGTCGCTACT
TCCCAAGAAGCTGTTCAGAATCAGAATGAGCCGCAACTTCGGGATGAAAATGCTCACAATGACAAATCTG
TCCACGGAGTGCTTAATCCAACTTACCAAGCTGGGTTACGACGCGACGCCGTTCAACCAGATATTGAAGC
AGAACGCAAAAAGAGAGATGAGATTGAGGCTGGGAAAAGTTACTGTAGCCGACGTTTTGGCGGCGCAACC
TGTGACGACAAATCTGCTCAAATTTATGCGCGCTTCGATAAAAATGATTGGCGTATCCAACCTGCA"









trinitydata=($( ls results.trinity/ ))

for arrayposition in ${!trinitydata[*]}

do


##########################################################################################################################
##############################                   Round 1 mapping - for proper quantitation				   ###############
##########################################################################################################################	

	###associate fluidigm barcode number with the well position in the original plate#####
	## 
	##	[well.info] - tab delimited text file with the following information
	##		1			2		3		4			5		6				7					8	
	##		fastqfile	well	type	Unspliced	cDNA	ExonicSequence	intron1_included	intron2_included
	## code that identifies line in well.info file that corresponds to the well of interest
	#	awkline=$(grep "${r1reads[$arrayposition]%%_*}" well.info)
	# to print out a specific field from $awkline use 
	#		$(echo "$awkline" | awk '{print $1}' )

	awkline=$(grep "${trinitydata[$arrayposition]%%_*}" well.info)
	r1reads=($(  ls fastq.trimmed/ | grep "${trinitydata[$arrayposition]%%_*}.*R1.*fq$" ))
	r2reads=($(  ls fastq.trimmed/ | grep "${trinitydata[$arrayposition]%%_*}.*R2.*fq$" ))
	
	
# add exon skipping sequence to end of Trinity.fa
# add cryptic exon sequence to end of Trinity.fa


	mkdir results.bowtie2_mapping/${trinitydata[$arrayposition]}

	#old trinity
	printf "${exonskip}\n" | cat results.trinity/${trinitydata[$arrayposition]}/Trinity-GG.fasta - > results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta
	# perform contamination check using fasta file in bowtie_quant folder
	
	echo 	"
	
	Beginning contamination check results.trinity/"$folder"_trinity/Trinity-GG.fasta
	
	"
	blat results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta mapping_indexes/reference_exon.fa	 -t=dna output.psl

	psl2sam.pl output.psl > results.contamination/${trinitydata[$arrayposition]}_output.sam

	rm output.psl 		
	
	printf "${phiX_reference}\n" >> results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta

	printf "${crypticExon}\n" >> results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta
	
	echo "
	*******************************************************
	***   Beginning Round 1 Mapping for Quantitation    ***
	*******************************************************
	going to combine results.trinity/${trinitydata[$arrayposition]}/Trinity-GG.fasta with exon skipping sequence into results.bowtie2_mapping/${trinitydata[$arrayposition]}	
	"
	
## build a bowtie2 index
	echo "		building a bowtie2 index for round 1 mapping"
	bowtie2-build -q results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}
		

## map reads with bowtie2, NOT using local mode, this is without k to get best isoform quantitation, will repeat again using -k 2 
	echo "		mapping with bowtie2 in round 1 mapping"
	printf "************mapping $r1reads and  $r2reads against Trinity assembly in results.trinity/${trinitydata[$arrayposition]}\n\n" >> bowtie2.out2

		
## running bowtie 
## v5 
##	- piped bowtie2 output direct into samtools instead of generating a sam file first
##  -S  results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.sam
## 	- had to remove   1>> bowtie2.out1  since bowtie2 streams output to stdout


	bowtie2 -5 5 -3 5 -p $numbOfCores -x results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]} 2>>bowtie2.out2 -1 fastq.trimmed/$r1reads -2 fastq.trimmed/$r2reads  | samtools view -bS - | samtools sort -  results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.sort
	
	samtools index results.bowtie2_mapping/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.sort.bam


##########################################################################################################################
##############################Round 2 mapping - includes well constructs and exons for visualization in IGV###############
##########################################################################################################################	


#	add well position cdna, unspliced, intron1_included and intron2_included.fa to end of trinity fasta
	###associate fluidigm barcode number with the well position in the original plate#####
	## 
	##	[well.info] - tab delimited text file with the following information
	##		1			2		3		4				5			6					7					8	
	##		fastqfile	well	type	Unspliced.fa	cDNA.fa		ExonicSequence.fa	intron1_included.fa	intron2_included.fa

# add exon skipping sequence, unspliced, cdna, intron1_included and intron2_included to end of Trinity.fa
# if type is cells, GFP, or pSPL3 then only print exonskip

	 echo "	
	 *******************************************************
	 ***       Beginning Round 2 Mapping for IGV         ***
	*******************************************************"
	 mkdir results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/

	 
	## determine if the dataset is an experimental well, or one of the multiple negative controls
	## if it is a negative control, then read the unspliced pSPL3 vector construct sequence in for the unspliced dataset
	if [[ $(echo "$awkline" | awk '{print $3}') =~ ^B ]] 
			then
				echo "$(echo "$awkline" | awk '{print $3}') Experimental"
				printf ">unspliced\n$(echo "$awkline" | awk '{print $4}') \n>cDNA\n$(echo "$awkline" | awk '{print $5}') \n>intron1_included\n$(echo "$awkline" | awk '{print $7}')\n>intron2_included\n$(echo "$awkline" | awk '{print $8}')\n" > temp.fa
				printf "${exonskip}\n${crypticExon}\n" | cat results.trinity/${trinitydata[$arrayposition]}/Trinity-GG.fasta temp.fa - > results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta	
			else
				## negative control, read the exonskip pSPL3 vector construct sequence in 
				echo "$(echo "$awkline" | awk '{print $3}') pSL3 exon skip entry"
				printf "${exonskip}\n${crypticExon}\n" | cat results.trinity/${trinitydata[$arrayposition]}/Trinity-GG.fasta - > results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta	
	fi	 

	printf "${crypticExon}\n" >> results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta
	
# ## build a bowtie2 index
	 echo "		building a bowtie2 index for igv"
	bowtie2-build -q results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}
		
# # map reads with bowtie2, NOT using local mode, clipping 10bp from each end, this is with k to get best isoform visualization
## had to remove 1>> bowtie2_igv.out1 to pipe results directly into stdout
	 echo "		mapping with bowtie2 for igv"
	 printf "************mapping $r1reads and  $r2reads against Trinity assembly in results.trinity/${trinitydata[$arrayposition]}\n\n" >> bowtie2_igv.out2

	bowtie2 -5 5 -3 5 -x results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]} -p $numbOfCores -1 fastq.trimmed/$r1reads -2 fastq.trimmed/$r2reads  -k 2   2>> bowtie2_igv.out2   | samtools view -bS - | samtools sort -  results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/reads_${trinitydata[$arrayposition]}.sort

	samtools index results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/reads_${trinitydata[$arrayposition]}.sort.bam

# ##########################################################################################################################
# ##############################Round 3 mapping - blat for visualization of reference transcripts in IGV     ###############
# ##########################################################################################################################	
# ##blat reference will have exon skipping, cdna, and unspliced transcript, , which will be the Trinity-GG.fasta file created for bowtie2 mapping
	# # include blank spaces at end of fasta lines
	# # blat database query [-ooc=11.ooc] output.psl
	# # 
# #	add well position cdna, unspliced, intron1_included and intron2_included.fa to end of trinity fasta
	# ###associate fluidigm barcode number with the well position in the original plate#####
	# ## 
	# ##	[well.info] - tab delimited text file with the following information
	# ##		1			2		3		4				5			6					7					8	
	# ##		fastqfile	well	type	Unspliced.fa	cDNA.fa		ExonicSequence.fa	intron1_included.fa	intron2_included.fa	
	# ## read out field of interest using $(echo "$awkline" | awk '{print $x}')
	# ## mapping with BLAT for visualizing differences in transcripts
	# ##
	# ## temp.fa from line 115 has unspliced, cdna, intron1_included, and intron2_included sequence
		# # only need to add vector exons, ExonicSequence.fa
	
	 echo "Beginning round 3 mapping for visualization of reference transcripts in IGV using BLAT"


	# #two BLAT reference files, reference_construct and reference_trinity
	# # reference_construct = BLAT [database]=results.bowtie2_mapping_igv/folder/trinity_well.fasta [query] = temp.fa

	if [[ $(echo "$awkline" | awk '{print $3}') =~ ^B ]] 
			then
				printf "$vectorexons\n>brca_exon\n$(echo "$awkline" | awk '{print $6}') \n"  | cat temp.fa - > temp.blat.fasta	
			else
				## negative control, only requires vector exons
				printf "$vectorexons\n"  | cat temp.fa - > temp.blat.fasta	
			fi	 
	 blat results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta temp.blat.fasta -t=dna output.psl
	
	 psl2sam.pl output.psl > output.sam
	
	 samtools faidx results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta
	
	 samtools view -bt results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta.fai output.sam > output.bam
	
	 samtools sort output.bam results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/reference_construct.${trinitydata[$arrayposition]}.sort
	
	 samtools index results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/reference_construct.${trinitydata[$arrayposition]}.sort.bam

	  	
	# # referent_trinity = BLAT [database]=results.bowtie2_mapping_igv/folder/trinity_well.fasta [query] results.trinity/folder/Trinity-GG.fasta
	
	 blat results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta results.trinity/${trinitydata[$arrayposition]}/Trinity-GG.fasta -t=dna output.psl
	
	 psl2sam.pl output.psl > output.sam
	
	 samtools faidx results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta
	
	samtools view -bt results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/${trinitydata[$arrayposition]}.fasta.fai output.sam > output.bam
	
	samtools sort output.bam results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/reference_trinity.${trinitydata[$arrayposition]}.sort
	
	samtools index results.bowtie2_mapping_igv/${trinitydata[$arrayposition]}/reference_trinity.${trinitydata[$arrayposition]}.sort.bam
	
done

rm temp.blat.fasta output.psl output.sam output.bam temp.fa

IFS=$SAVEIFS	
