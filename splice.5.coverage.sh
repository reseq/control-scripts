##Author: Darren Korbie
resultsfolderBt2=($(  ls results.bowtie2_mapping ))

echo "" > summary.coverage_and_cigarValues.tab.txt


#read file names into array

for arrayposition in ${!resultsfolderBt2[*]}

do

	echo "processing results.bowtie2_mapping_igv/reads_${resultsfolderBt2[$arrayposition]}.sort.bam"
	
	#echo " results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/reference_construct.${resultsfolderBt2[$arrayposition]}.sort.bam"
	samtools view results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/reference_construct.${resultsfolderBt2[$arrayposition]}.sort.bam > results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/reference_construct.${resultsfolderBt2[$arrayposition]}.sort.sam
	
	samtools view results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/reference_trinity.${resultsfolderBt2[$arrayposition]}.sort.bam > results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/reference_trinity.${resultsfolderBt2[$arrayposition]}.sort.sam

	
	
	## generate bed file from transcripts list
	
	 awk ' BEGIN {OFS = "\t"; getline; split($0,array," "); printf substr(array[1],2) "\t"}
	 
		{
		if (/^>/) { printf "1\t" fastalength "\n"; split($0,array," "); printf substr(array[1],2) "\t"; fastalength=0}
		else  {fastalength+=length($0)}
		
		}
		END {printf "1\t" fastalength "\n"}
		' results.bowtie2_mapping/${resultsfolderBt2[$arrayposition]}/${resultsfolderBt2[$arrayposition]}.fasta > temp.bed

	#echo "TRcrypticExon\t1\t296" >> temp.bed
		
	# # use bedtools to determine read coverage

	match=($( echo ${resultsfolderBt2[$arrayposition]} | awk '{ split($0,array,"_"); print array[1]}'))

	readnum=($(grep $match summary.bowtie2_quant_mapping.tab.txt | awk '{print $5}'	))



	filename=($(echo results.bowtie2_mapping_igv/${resultsfolderBt2[$arrayposition]}/reference_trinity.${resultsfolderBt2[$arrayposition]}.sort.sam))
	
	echo $filename >> summary.coverage_and_cigarValues.tab.txt
	 
	  coverageBed -abam results.bowtie2_mapping/${resultsfolderBt2[$arrayposition]}/${resultsfolderBt2[$arrayposition]}.sort.bam -b temp.bed  	| awk  ' BEGIN {print "chr\tstart\tstop\treads\tbases_covered\t% of lib"} {
	   OFS = "\t";OFMT = "%.2f";
	   successfulLoop = 0
	   transcriptID = $1; one = $1; two = $2; three = $3; four = $4; five = $5; six = $4/2/"'${readnum}'"*100;
	   while(( getline  < "'${filename}'") >0 )
		if ($1 == transcriptID && $3 == "cDNA") {print one, two, three, four, five, six,  "CIGAR string against cDNA is", $6; successfulLoop = 1}
		
		close("'${filename}'")
		
		if (successfulLoop == 0) {print one, two, three, four, five, six, "unmapped against cDNA"}
		
	}' >> summary.coverage_and_cigarValues.tab.txt
	
	
done
